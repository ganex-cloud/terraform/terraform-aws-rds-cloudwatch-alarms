# Variables

variable "sns_topic_arn" {
  description = "A list of ARNs (i.e. SNS Topic ARN) to notify on alerts"
  type        = list(string)
}

variable "alarm_name_prefix" {
  description = "Alarm name prefix"
  type        = string
  default     = ""
}

variable "db_instance_id" {
  description = "The instance ID of the RDS database instance that you want to monitor."
  type        = string
}

variable "burst_balance_threshold" {
  description = "The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available."
  type        = string
  default     = 20
}

variable "ebs_byte_balance_threshold" {
  description = "The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available for EBS byte balance."
  type        = string
  default     = 30
}

variable "ebs_io_balance_threshold" {
  description = "The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available for EBS I/O balance."
  type        = string
  default     = 30
}

variable "cpu_utilization_threshold" {
  description = "The maximum percentage of CPU utilization."
  type        = string
  default     = 80
}

variable "cpu_credit_balance_threshold" {
  description = "The minimum number of CPU credits (t2 instances only) available."
  type        = string
  default     = 20
}

variable "disk_queue_depth_threshold" {
  description = "The maximum number of outstanding IOs (read/write requests) waiting to access the disk."
  type        = string
  default     = 64
}

variable "freeable_memory_threshold" {
  description = "The minimum amount of available random access memory in Byte."
  type        = string
  default     = 160000000 # 160 Megabyte in Byte
}

variable "free_storage_space_threshold" {
  description = "The minimum amount of available storage space in Byte."
  type        = string
  default     = 2000000000 # 2 Gigabyte in Byte
}

variable "swap_usage_threshold" {
  description = "The maximum amount of swap space used on the DB instance in Byte."
  type        = string
  default     = 512000000 # 256 Megabyte in Byte
}

variable "db_connections_threshold" {
  description = "The maximum number of connections on the DB instance."
  type        = string
  default     = 200
}

variable "replica_lag_threshold" {
  description = "The maximum replication lag in seconds."
  type        = string
  default     = 10
}

variable "oldest_replication_slot_threshold" {
  description = "The maximum lag of the oldest replication slot in bytes."
  type        = string
  default     = 1073741824
}

variable "burst_balance_too_low-alarm" {
  description = "Enable Alarm for burst balance being too low"
  default     = true
  type        = bool
}

variable "burst_balance_too_low-comparison_operator" {
  description = "Comparison operator for burst balance alarm"
  default     = "LessThanThreshold"
}

variable "burst_balance_too_low-datapoint" {
  description = "Datapoint check for burst balance alarm"
  default     = "1"
}

variable "burst_balance_too_low-period" {
  description = "Period check for burst balance alarm (in seconds)"
  default     = "600"
}

variable "burst_balance_too_low-priority" {
  description = "Priority of burst balance alarm"
  default     = "P3"
  type        = string
}

variable "ebs_byte_balance_too_low-alarm" {
  description = "Enable Alarm for EBS byte balance being too low"
  default     = true
  type        = bool
}

variable "ebs_byte_balance_too_low-comparison_operator" {
  description = "Comparison operator for EBS byte balance alarm"
  default     = "LessThanThreshold"
}

variable "ebs_byte_balance_too_low-datapoint" {
  description = "Datapoint check for EBS byte balance alarm"
  default     = "1"
}

variable "ebs_byte_balance_too_low-period" {
  description = "Period check for EBS byte balance alarm (in seconds)"
  default     = "600"
}

variable "ebs_byte_balance_too_low-priority" {
  description = "Priority of EBS byte balance alarm"
  default     = "P3"
  type        = string
}

variable "ebs_io_balance_too_low-alarm" {
  description = "Enable Alarm for EBS I/O balance being too low"
  default     = true
  type        = bool
}

variable "ebs_io_balance_too_low-comparison_operator" {
  description = "Comparison operator for EBS I/O balance alarm"
  default     = "LessThanThreshold"
}

variable "ebs_io_balance_too_low-datapoint" {
  description = "Datapoint check for EBS I/O balance alarm"
  default     = "1"
}

variable "ebs_io_balance_too_low-period" {
  description = "Period check for EBS I/O balance alarm (in seconds)"
  default     = "600"
}

variable "ebs_io_balance_too_low-priority" {
  description = "Priority of EBS I/O balance alarm"
  default     = "P3"
  type        = string
}

variable "cpu_utilization_too_high-alarm" {
  description = "Enable Alarm for CPU utilization being too high"
  default     = true
  type        = bool
}

variable "cpu_utilization_too_high-comparison_operator" {
  description = "Comparison operator for CPU utilization alarm"
  default     = "GreaterThanThreshold"
}

variable "cpu_utilization_too_high-datapoint" {
  description = "Datapoint check for CPU utilization alarm"
  default     = "1"
}

variable "cpu_utilization_too_high-period" {
  description = "Period check for CPU utilization alarm (in seconds)"
  default     = "600"
}

variable "cpu_utilization_too_high-priority" {
  description = "Priority of CPU utilization alarm"
  default     = "P3"
  type        = string
}

variable "cpu_credit_balance_too_low-alarm" {
  description = "Enable Alarm for CPU credit balance being too low"
  default     = true
  type        = bool
}

variable "cpu_credit_balance_too_low-comparison_operator" {
  description = "Comparison operator for CPU credit balance alarm"
  default     = "LessThanThreshold"
}

variable "cpu_credit_balance_too_low-datapoint" {
  description = "Datapoint check for CPU credit balance alarm"
  default     = "1"
}

variable "cpu_credit_balance_too_low-period" {
  description = "Period check for CPU credit balance alarm (in seconds)"
  default     = "600"
}

variable "cpu_credit_balance_too_low-priority" {
  description = "Priority of CPU credit balance alarm"
  default     = "P3"
  type        = string
}

variable "disk_queue_depth_too_high-alarm" {
  description = "Enable Alarm for disk queue depth being too high"
  default     = true
  type        = bool
}

variable "disk_queue_depth_too_high-comparison_operator" {
  description = "Comparison operator for disk queue depth alarm"
  default     = "GreaterThanThreshold"
}

variable "disk_queue_depth_too_high-datapoint" {
  description = "Datapoint check for disk queue depth alarm"
  default     = "1"
}

variable "disk_queue_depth_too_high-period" {
  description = "Period check for disk queue depth alarm (in seconds)"
  default     = "600"
}

variable "disk_queue_depth_too_high-priority" {
  description = "Priority of disk queue depth alarm"
  default     = "P4"
  type        = string
}

variable "freeable_memory_too_low-alarm" {
  description = "Enable Alarm for freeable memory being too low"
  default     = true
  type        = bool
}

variable "freeable_memory_too_low-comparison_operator" {
  description = "Comparison operator for freeable memory alarm"
  default     = "LessThanThreshold"
}

variable "freeable_memory_too_low-datapoint" {
  description = "Datapoint check for freeable memory alarm"
  default     = "1"
}

variable "freeable_memory_too_low-period" {
  description = "Period check for freeable memory alarm (in seconds)"
  default     = "600"
}

variable "freeable_memory_too_low-priority" {
  description = "Priority of freeable memory alarm"
  default     = "P4"
  type        = string
}

variable "free_storage_space_too_low-alarm" {
  description = "Enable Alarm for free storage space being too low"
  default     = true
  type        = bool
}

variable "free_storage_space_too_low-comparison_operator" {
  description = "Comparison operator for free storage space alarm"
  default     = "LessThanThreshold"
}

variable "free_storage_space_too_low-datapoint" {
  description = "Datapoint check for free storage space alarm"
  default     = "1"
}

variable "free_storage_space_too_low-period" {
  description = "Period check for free storage space alarm (in seconds)"
  default     = "600"
}

variable "free_storage_space_too_low-priority" {
  description = "Priority of free storage space alarm"
  default     = "P3"
  type        = string
}

variable "swap_usage_too_high-alarm" {
  description = "Enable Alarm for swap usage being too high"
  default     = true
  type        = bool
}

variable "swap_usage_too_high-comparison_operator" {
  description = "Comparison operator for swap usage alarm"
  default     = "GreaterThanThreshold"
}

variable "swap_usage_too_high-datapoint" {
  description = "Datapoint check for swap usage alarm"
  default     = "1"
}

variable "swap_usage_too_high-period" {
  description = "Period check for swap usage alarm (in seconds)"
  default     = "600"
}

variable "swap_usage_too_high-priority" {
  description = "Priority of swap usage alarm"
  default     = "P4"
  type        = string
}

variable "db_connections_too_high-alarm" {
  description = "Enable Alarm for database connections being too high"
  default     = "false"
}

variable "db_connections_too_high-comparison_operator" {
  description = "Comparison operator for database connections alarm"
  default     = "GreaterThanThreshold"
}

variable "db_connections_too_high-datapoint" {
  description = "Datapoint check for database connections alarm"
  default     = "1"
}

variable "db_connections_too_high-period" {
  description = "Period check for database connections alarm (in seconds)"
  default     = "600"
}

variable "db_connections_too_high-priority" {
  description = "Priority of database connections alarm"
  default     = "P4"
  type        = string
}

variable "replica_lag_too_high-alarm" {
  description = "Enable Alarm for replica lag being too high"
  default     = "false"
}

variable "replica_lag_too_high-comparison_operator" {
  description = "Comparison operator for replica lag alarm"
  default     = "GreaterThanThreshold"
}

variable "replica_lag_too_high-datapoint" {
  description = "Datapoint check for replica lag alarm"
  default     = "1"
}

variable "replica_lag_too_high-period" {
  description = "Period check for replica lag alarm (in seconds)"
  default     = "60"
}

variable "replica_lag_too_high-priority" {
  description = "Priority of replica lag alarm"
  default     = "P2"
  type        = string
}

variable "oldest_replication_slot_lag_too_high-alarm" {
  description = "Enable Alarm for oldest replication slot lag being too high"
  default     = "false"
}

variable "oldest_replication_slot_lag_too_high-comparison_operator" {
  description = "Comparison operator for oldest replication slot lag alarm"
  default     = "GreaterThanThreshold"
}

variable "oldest_replication_slot_lag_too_high-datapoint" {
  description = "Datapoint check for oldest replication slot lag alarm"
  default     = "1"
}

variable "oldest_replication_slot_lag_too_high-period" {
  description = "Period check for oldest replication slot lag alarm (in seconds)"
  default     = "60"
}

variable "oldest_replication_slot_lag_too_high-priority" {
  description = "Priority of oldest replication slot lag alarm"
  default     = "P3"
  type        = string
}

variable "tags" {
  description = "(Optional) A map of tags to assign to all resources"
  type        = map(string)
  default     = {}
}
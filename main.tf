data "aws_iam_account_alias" "current" {}

locals {
  alarm_name_prefix = title(var.alarm_name_prefix == "" ? data.aws_iam_account_alias.current.account_alias : var.alarm_name_prefix)
  thresholds = {
    BurstBalanceThreshold             = min(max(var.burst_balance_threshold, 0), 100)
    CPUUtilizationThreshold           = min(max(var.cpu_utilization_threshold, 0), 100)
    CPUCreditBalanceThreshold         = max(var.cpu_credit_balance_threshold, 0)
    DiskQueueDepthThreshold           = max(var.disk_queue_depth_threshold, 0)
    FreeableMemoryThreshold           = max(var.freeable_memory_threshold, 0)
    FreeStorageSpaceThreshold         = max(var.free_storage_space_threshold, 0)
    SwapUsageThreshold                = max(var.swap_usage_threshold, 0)
    DBConnectionsThreshold            = max(var.db_connections_threshold, 0)
    ReplicaLagThreshold               = max(var.replica_lag_threshold, 0)
    OldestReplicationSlotLagThreshold = max(var.oldest_replication_slot_threshold, 0)
    EBSByteBalanceThreshold           = min(max(var.ebs_byte_balance_threshold, 0), 100)
    EBSIOBalanceThreshold             = min(max(var.ebs_io_balance_threshold, 0), 100)
  }
}

resource "aws_cloudwatch_metric_alarm" "burst_balance_too_low" {
  count               = var.burst_balance_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-burst-balance-too-low"
  comparison_operator = var.burst_balance_too_low-comparison_operator
  evaluation_periods  = var.burst_balance_too_low-datapoint
  metric_name         = "BurstBalance"
  namespace           = "AWS/RDS"
  period              = var.burst_balance_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["BurstBalanceThreshold"]
  alarm_description   = "Average RDS instance burst balance is critically low over the last ${var.burst_balance_too_low-period} minutes. This indicates that the instance has depleted its I/O credits and may experience severely degraded performance. Immediate action is required to prevent potential service disruptions. (${var.burst_balance_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_too_high" {
  count               = var.cpu_utilization_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-cpu-utilization-too-high"
  comparison_operator = var.cpu_utilization_too_high-comparison_operator
  evaluation_periods  = var.cpu_utilization_too_high-datapoint
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = var.cpu_utilization_too_high-period
  statistic           = "Average"
  threshold           = local.thresholds["CPUUtilizationThreshold"]
  alarm_description   = "Average RDS instance CPU utilization is too high over the last ${var.cpu_utilization_too_high-period} minutes. This may indicate performance issues or the need for scaling. Consider optimizing queries or increasing instance capacity. (${var.cpu_utilization_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_credit_balance_too_low" {
  count               = var.cpu_credit_balance_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-cpu-credit-balance-too-low"
  comparison_operator = var.cpu_credit_balance_too_low-comparison_operator
  evaluation_periods  = var.cpu_credit_balance_too_low-datapoint
  metric_name         = "CPUCreditBalance"
  namespace           = "AWS/RDS"
  period              = var.cpu_credit_balance_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["CPUCreditBalanceThreshold"]
  alarm_description   = "Average RDS instance CPU credit balance is critically low over the last ${var.cpu_credit_balance_too_low-period} minutes. This may lead to significant performance degradation for T2/T3 instances. Consider upgrading to a larger instance type or switching to a non-burstable instance. (${var.cpu_credit_balance_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "disk_queue_depth_too_high" {
  count               = var.disk_queue_depth_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-disk-queue-depth-too-high"
  comparison_operator = var.disk_queue_depth_too_high-comparison_operator
  evaluation_periods  = var.disk_queue_depth_too_high-datapoint
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/RDS"
  period              = var.disk_queue_depth_too_high-period
  statistic           = "Average"
  threshold           = local.thresholds["DiskQueueDepthThreshold"]
  alarm_description   = "Average RDS instance disk queue depth is too high over the last ${var.disk_queue_depth_too_high-period} minutes. This indicates I/O congestion and may result in increased latency. Consider optimizing disk I/O operations or upgrading to a storage type with higher IOPS. (${var.disk_queue_depth_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "freeable_memory_too_low" {
  count               = var.freeable_memory_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-freeable-memory-too-low"
  comparison_operator = var.freeable_memory_too_low-comparison_operator
  evaluation_periods  = var.freeable_memory_too_low-datapoint
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period              = var.freeable_memory_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["FreeableMemoryThreshold"]
  alarm_description   = "Average RDS instance freeable memory is critically low over the last ${var.freeable_memory_too_low-period} minutes. This may lead to performance issues or potential out-of-memory errors. Consider optimizing memory usage, increasing instance size, or adding more RAM. (${var.freeable_memory_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "free_storage_space_too_low" {
  count               = var.free_storage_space_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-free-storage-space-threshold"
  comparison_operator = var.free_storage_space_too_low-comparison_operator
  evaluation_periods  = var.free_storage_space_too_low-datapoint
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = var.free_storage_space_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["FreeStorageSpaceThreshold"]
  alarm_description   = "Average RDS instance free storage space is critically low over the last ${var.free_storage_space_too_low-period} minutes. This may lead to database write failures or corruption. Urgent action required: clean up unnecessary data, increase allocated storage, or set up auto-scaling storage. (${var.free_storage_space_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "swap_usage_too_high" {
  count               = var.swap_usage_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-swap-usage-too-high"
  comparison_operator = var.swap_usage_too_high-comparison_operator
  evaluation_periods  = var.swap_usage_too_high-datapoint
  metric_name         = "SwapUsage"
  namespace           = "AWS/RDS"
  period              = var.swap_usage_too_high-period
  statistic           = "Average"
  threshold           = local.thresholds["SwapUsageThreshold"]
  alarm_description   = "Average RDS instance swap usage is too high over the last ${var.swap_usage_too_high-period} minutes. This indicates memory pressure and can severely impact performance. Investigate memory-intensive processes and consider increasing instance memory. (${var.swap_usage_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "db_connections_too_high" {
  count               = var.db_connections_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-db-connections-too-high"
  comparison_operator = var.db_connections_too_high-comparison_operator
  evaluation_periods  = var.db_connections_too_high-datapoint
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = var.db_connections_too_high-period
  statistic           = "Average"
  threshold           = local.thresholds["DBConnectionsThreshold"]
  alarm_description   = "Average RDS instance database connections are too high over the last ${var.db_connections_too_high-period} minutes. This may indicate connection leaks or inefficient connection management. Review application code for proper connection handling and consider implementing connection pooling. (${var.db_connections_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "replica_lag_too_high" {
  count               = var.replica_lag_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-replica-lag-too-high"
  comparison_operator = var.replica_lag_too_high-comparison_operator
  evaluation_periods  = var.replica_lag_too_high-datapoint
  metric_name         = "ReplicaLag"
  namespace           = "AWS/RDS"
  period              = var.replica_lag_too_high-period
  statistic           = "Maximum"
  threshold           = local.thresholds["ReplicaLagThreshold"]
  alarm_description   = "RDS replica lag is too high over the last ${var.replica_lag_too_high-period} seconds. This may lead to data inconsistencies and impact read scaling. Investigate reasons for replication delays and consider optimizing network connectivity or increasing instance capacity. (${var.replica_lag_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "breaching"

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "oldest_replication_slot_lag_too_high" {
  count               = var.oldest_replication_slot_lag_too_high-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-oldest-replication-slot-lag-too-high"
  comparison_operator = var.oldest_replication_slot_lag_too_high-comparison_operator
  evaluation_periods  = var.oldest_replication_slot_lag_too_high-datapoint
  metric_name         = "OldestReplicationSlotLag"
  namespace           = "AWS/RDS"
  period              = var.oldest_replication_slot_lag_too_high-period
  statistic           = "Maximum"
  threshold           = local.thresholds["OldestReplicationSlotLagThreshold"]
  alarm_description   = "RDS oldest replication slot lag is too high over the last ${var.oldest_replication_slot_lag_too_high-period} seconds. This may indicate issues with logical replication or long-running transactions. Review replication processes and consider optimizing long-running queries. (${var.oldest_replication_slot_lag_too_high-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "breaching"

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "ebs_byte_balance_too_low" {
  count               = var.ebs_byte_balance_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-ebs-byte-balance-too-low"
  comparison_operator = var.ebs_byte_balance_too_low-comparison_operator
  evaluation_periods  = var.ebs_byte_balance_too_low-datapoint
  metric_name         = "EBSByteBalance%"
  namespace           = "AWS/RDS"
  period              = var.ebs_byte_balance_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["EBSByteBalanceThreshold"]
  alarm_description   = "Average RDS instance EBS byte balance is critically low over the last ${var.ebs_byte_balance_too_low-period} minutes. This indicates potential I/O throttling for gp2 volumes. Consider upgrading to gp3 or provisioned IOPS storage for consistent performance. (${var.ebs_byte_balance_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "ebs_io_balance_too_low" {
  count               = var.ebs_io_balance_too_low-alarm ? 1 : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${var.db_instance_id}-ebs-io-balance-too-low"
  comparison_operator = var.ebs_io_balance_too_low-comparison_operator
  evaluation_periods  = var.ebs_io_balance_too_low-datapoint
  metric_name         = "EBSIOBalance%"
  namespace           = "AWS/RDS"
  period              = var.ebs_io_balance_too_low-period
  statistic           = "Average"
  threshold           = local.thresholds["EBSIOBalanceThreshold"]
  alarm_description   = "Average RDS instance EBS I/O balance is critically low over the last ${var.ebs_io_balance_too_low-period} minutes. This indicates potential I/O throttling for gp2 volumes. Consider upgrading to gp3 or provisioned IOPS storage for higher I/O performance. (${var.ebs_io_balance_too_low-priority})"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = var.db_instance_id
  }
}

# terraform-aws-rds-cloudwatch-alarms

This Terraform module configures important RDS alerts with CloudWatch and SNS topic integration.

## Usage

```hcl
module "rds_alarms" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-rds-cloudwatch-alarms.git?ref=master"
  db_instance_id = module.rds-instance.id
  sns_topic_arn  = ["arn:aws:sns:region:account-id:topic-name"]
}
```

## Alarms

This module sets up the following CloudWatch alarms for your RDS instance:

| Alarm                                | Metric                   | Default Threshold | Description                                           |
| ------------------------------------ | ------------------------ | ----------------- | ----------------------------------------------------- |
| Burst Balance Too Low                | BurstBalance             | < 20%             | Indicates low I/O credits for gp2 volumes             |
| CPU Utilization Too High             | CPUUtilization           | > 80%             | High CPU usage may indicate performance issues        |
| CPU Credit Balance Too Low           | CPUCreditBalance         | < 20              | Low CPU credits for T2/T3 instances                   |
| Disk Queue Depth Too High            | DiskQueueDepth           | > 64              | Indicates I/O congestion                              |
| Freeable Memory Too Low              | FreeableMemory           | < 160 MB          | Low available memory may cause performance issues     |
| Free Storage Space Too Low           | FreeStorageSpace         | < 2 GB            | Low storage space may lead to write failures          |
| Swap Usage Too High                  | SwapUsage                | > 512 MB          | High swap usage indicates memory pressure             |
| Database Connections Too High        | DatabaseConnections      | > 200             | High number of connections may indicate issues        |
| Replica Lag Too High                 | ReplicaLag               | > 10 seconds      | High replication lag may lead to data inconsistencies |
| Oldest Replication Slot Lag Too High | OldestReplicationSlotLag | > 1 GB            | Issues with logical replication                       |
| EBS Byte Balance Too Low             | EBSByteBalance%          | < 30%             | Low I/O credits for gp2 volumes                       |
| EBS IO Balance Too Low               | EBSIOBalance%            | < 30%             | Low I/O credits for gp2 volumes                       |

## Inputs

| Name                              | Description                                                                                              |     Type     |  Default   | Required |
| --------------------------------- | -------------------------------------------------------------------------------------------------------- | :----------: | :--------: | :------: |
| sns_topic_arn                     | A list of ARNs (i.e. SNS Topic ARN) to notify on alerts                                                  | list(string) |     -      |   yes    |
| db_instance_id                    | The instance ID of the RDS database instance to monitor                                                  |    string    |     -      |   yes    |
| alarm_name_prefix                 | Prefix for alarm names                                                                                   |    string    |     ""     |    no    |
| burst_balance_threshold           | The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available                      |    number    |     20     |    no    |
| cpu_utilization_threshold         | The maximum percentage of CPU utilization                                                                |    number    |     80     |    no    |
| cpu_credit_balance_threshold      | The minimum number of CPU credits (T2/T3 instances only) available                                       |    number    |     20     |    no    |
| disk_queue_depth_threshold        | The maximum number of outstanding IOs (read/write requests) waiting to access the disk                   |    number    |     64     |    no    |
| freeable_memory_threshold         | The minimum amount of available random access memory in bytes                                            |    number    | 160000000  |    no    |
| free_storage_space_threshold      | The minimum amount of available storage space in bytes                                                   |    number    | 2000000000 |    no    |
| swap_usage_threshold              | The maximum amount of swap space used on the DB instance in bytes                                        |    number    | 512000000  |    no    |
| db_connections_threshold          | The maximum number of database connections                                                               |    number    |    200     |    no    |
| replica_lag_threshold             | The maximum replication lag in seconds                                                                   |    number    |     10     |    no    |
| oldest_replication_slot_threshold | The maximum lag of the oldest replication slot in bytes                                                  |    number    | 1073741824 |    no    |
| ebs_byte_balance_threshold        | The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available for EBS byte balance |    number    |     30     |    no    |
| ebs_io_balance_threshold          | The minimum percent of General Purpose SSD (gp2) burst-bucket I/O credits available for EBS I/O balance  |    number    |     30     |    no    |

Note: Each alarm has additional configuration options for enabling/disabling, comparison operator, evaluation periods, and priority. Refer to the `variables.tf` file for a complete list of available options.

## Tags

You can add tags to all resources created by this module by providing a map of tag keys and values:

```hcl
module "rds_alarms" {
  // ... other configuration ...
  tags = {
    Environment = "Production"
    Project     = "MyApp"
  }
}
```

For more detailed information on each variable and its usage, please refer to the `variables.tf` file in the module.
